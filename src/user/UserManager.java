package user;

import database.DbConnectionManager;
import database.UserDAO;

public class UserManager {

	
	public User getUser(String userName) {

		UserDAO userDao = new UserDAO(DbConnectionManager.getInstance());
		int user_id = userDao.getUserNameFromId(userName);
		
		User user = userDao.get(user_id);
		
		return user;
			
	}
	

}
