package activity;

import java.util.ArrayList;
import java.util.List;

import database.ActivityDAO;

public class ActivityManager {

	private Activity activity;
	
	public void addActivity(int id, String name, String path, int user_id) {
		this.activity = new Activity(id, name);
		this.activity.addActivityToDB(user_id);
		this.activity.importData(path);	
	}
	
	public List<Activity> getAllActivitys() {
		
		ActivityDAO activityDao = new ActivityDAO();
		List<Activity> activityList = new ArrayList<Activity>();
		activityList = activityDao.getAllActivitys();
		
		for(int i = 0; i < activityList.size(); i++) {
			activityList.get(i).setPointList();
		}
		
		return activityList;
	}
	
	
}
