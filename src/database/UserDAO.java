package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import activity.Activity;
import user.User;

public class UserDAO {
	
	private DbConnectionManager dbConManagerSingleton = null;
	
	
	public UserDAO(DbConnectionManager dbConManagerSingleton) {
		this.dbConManagerSingleton = dbConManagerSingleton;
	}
	

	public List<User> getAll() {
		List<User> userList = new ArrayList<User>();
		
		try {
			ResultSet resultSet = this.dbConManagerSingleton.excecuteQuery("SELECT user_id, user_pw, user_name FROM user_credentials");
			while(resultSet.next()) {
				User user = new User();
				user.setUserID(resultSet.getInt(1));
				user.setUserPW(resultSet.getString(2));
				user.setUserName(resultSet.getString(3));
			}
			this.dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return userList;
	}


	public User get(int id) {
		
		User user = null; 
		
		try {
			ResultSet resultSet = this.dbConManagerSingleton.excecuteQuery("SELECT user_id, user_pw, user_name FROM user_credentials WHERE user_id =" + id);
				user = new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3));
	
			this.dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
	public void delete(int id) {
		
		PreparedStatement preparedStatement = null; 
		try {
		preparedStatement = dbConManagerSingleton.prepareStatement("DELETE FROM user_credentials WHERE user_id = ? values(?)");
		
		preparedStatement.setInt(id, 1);
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	

	
	public int getUserNameFromId(String userName) {
		 
		int id = 0;
		
		try {
			ResultSet resultSet = this.dbConManagerSingleton.excecuteQuery("SELECT user_id FROM user_credentials WHERE user_name =" + userName);
			id = resultSet.getInt(1);
			
			this.dbConManagerSingleton.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return id;
	}
	
	/*public int addUser() {
		
		
	}*/

}
