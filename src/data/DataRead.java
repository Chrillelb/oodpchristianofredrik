package data;

//C:\\Users\\fredr\\OneDrive\\Dokument\\ObjektOrienteradProgramering\\Projekt\\CSV-filer\\csv\\CSV_TestFile.csv - Datas�kv�g
//C:\\Users\\chris\\OneDrive\\Skrivbord\\Skola\\Hösttermin2020\\Objektorienteraddesignochprogrammering\\csv\\CSV_TestFile.csv

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


import database.PointDAO;
import presentation.DataPresentation;
import trackPoint.TrackPoint;


public class DataRead {

	private String path;
	private String line = "";
	private TrackPoint ref;
	private ArrayList<TrackPoint> trackPointList = new ArrayList<TrackPoint>();
	
	public DataRead(String path) {
		this.path = path;
	}
	
	
	public ArrayList<TrackPoint> readFile() {

		 Scanner fileScanner = null;

	        try {
	            fileScanner = new Scanner(new FileReader(path));
	        } catch (FileNotFoundException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
		
	        fileScanner.nextLine();
	        
	        while (fileScanner.hasNextLine()) {
	            String temp[] = fileScanner.nextLine().split(";");
	            for(int i=0;i<temp.length;i++) {

	                temp[i]=temp[i].replace(",", ".");

	            }
	            trackPointList.add(new TrackPoint(temp[0], temp[1], Double.parseDouble(temp[2]),Double.parseDouble(temp[3]) , Double.parseDouble(temp[4]), Double.parseDouble(temp[5]), 
	                    Double.parseDouble(temp[6]), Integer.parseInt(temp[7]), Double.parseDouble(temp[8]), Integer.parseInt(temp[9])));
	        }
			
	        fileScanner.close();
	        
		return this.trackPointList;
	}

}
